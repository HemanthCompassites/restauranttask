package com.compassites.user.maplocation;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;


/**
 * Created by user on 5/5/2016.
 */
public class MapActivity extends AppCompatActivity implements View.OnClickListener {

    private GoogleMap myMap;
    Toolbar toolbar;
    ImageView mBackIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lyt_map_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mBackIcon = (ImageView) findViewById(R.id.iv_back);
        mBackIcon.setOnClickListener(this);
        toolbar.setTitleTextColor(Color.WHITE);

        FragmentManager myFragmentManager = getFragmentManager();
        MapFragment mySupportMapFragment = (MapFragment) myFragmentManager.findFragmentById(R.id.my_map);
        myMap = mySupportMapFragment.getMap();

        try {
            myMap.setMyLocationEnabled(true);
        } catch (SecurityException ex) {
            ex.printStackTrace();
        }
        myMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        Intent i = getIntent();
        Bundle extras = i.getExtras();

        if (extras != null && extras.containsKey("desLatitude") && extras.containsKey("desLongitude")) {
            String lat = extras.getString("desLatitude");
            String lng = extras.getString("desLongitude");


            //display marker of restaraunt when current location is null
            if (!extras.containsKey("currentLatitude")) {
                onDisplayMarker(Double.parseDouble(lat), Double.parseDouble(lng));
            } else {
                Double currentLat = extras.getDouble("currentLatitude");
                Double currentLng = extras.getDouble("currentLongitude");

                onDrawRoute(new LatLng(currentLat, currentLng), new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
        }
    }


    private void onDrawRoute(LatLng source, LatLng destination) {

        PolylineOptions routeDraw = new PolylineOptions().width(5).color(Color.BLUE);
        routeDraw.add(source);
        routeDraw.add(destination);
        Polyline polyline = myMap.addPolyline(routeDraw);
        CameraPosition position = new CameraPosition(destination, 8, 0, 0);
        myMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
        Marker marker = myMap.addMarker(new MarkerOptions()
                .position(destination)
                .title("You are Here"));
    }

    private void onDisplayMarker(Double lat, Double lng) {

        CameraPosition position = new CameraPosition(new LatLng(lat, lng), 11, 0, 0);
        myMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
        myMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)));
    }
}
