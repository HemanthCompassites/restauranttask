package com.compassites.user.maplocation;

import com.google.gson.JsonElement;
import com.squareup.okhttp.ResponseBody;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by user on 5/5/2016.
 */
public interface ApiMethodService {

    @GET("/tests/c2Utc3Nl")
    public Call<List<RestaurantModel>> getRestaurantData();

}
