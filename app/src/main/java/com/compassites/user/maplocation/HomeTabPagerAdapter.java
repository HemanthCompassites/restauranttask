package com.compassites.user.maplocation;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by user on 5/4/2016.
 */
public class HomeTabPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public HomeTabPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                BreakFastFragment tab1 = new BreakFastFragment();
                return tab1;
            case 1:
                LunchFragment tab2 = new LunchFragment();
                return tab2;
            case 2:
                LunchFragment tab3 = new LunchFragment();
                return tab3;
            case 3:
                LunchFragment tab4 = new LunchFragment();
                return tab4;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}