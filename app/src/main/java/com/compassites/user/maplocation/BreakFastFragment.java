package com.compassites.user.maplocation;


import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.util.List;
import java.util.Locale;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;

import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by user on 5/4/2016.
 */
public class BreakFastFragment extends Fragment implements View.OnClickListener {

    private RecyclerView mRecyclerView;
    private RestaurantCustomAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Location mCurrentLocation;

    ProgressDialog progressDialog;
    TextView txtCurrentAddress;
    RelativeLayout lytNoInternet;
    ImageView retryInternet;
    int locationCode;
    int responseCode;
    boolean isDataFetched;
    private List<RestaurantModel> mRestModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.lyt_breakfast_fragment, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
        txtCurrentAddress = (TextView) rootView.findViewById(R.id.txt_currentAddress);
        lytNoInternet = (RelativeLayout) rootView.findViewById(R.id.lyt_no_internet);
        retryInternet = (ImageView) rootView.findViewById(R.id.img_retry);
        retryInternet.setOnClickListener(this);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        try {
            locationCode = Settings.Secure.getInt(getActivity().getContentResolver(), Settings.Secure.LOCATION_MODE);
            if (locationCode == 0) {
                Utilities.displayPromptForEnablingGPS(getActivity());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (Utilities.checkInternetConnection(getActivity())) {
            lytNoInternet.setVisibility(View.GONE);
            if (mRestModel == null) {
                onLoadData();
            } else {
                setUpUI(mRestModel);
            }
        } else {
            lytNoInternet.setVisibility(View.VISIBLE);
        }

    }

    public void onLoadData() {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("loading");
        progressDialog.show();

        // prepare call in Retrofit 2.0
        ApiMethodService apiMethodService = RetrofitHelper.getRetrofitBuilder().create(ApiMethodService.class);


        Call<List<RestaurantModel>> call = apiMethodService.getRestaurantData();
        //asynchronous call
        call.enqueue(new Callback<List<RestaurantModel>>() {
            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
            }

            @Override
            public void onResponse(Response<List<RestaurantModel>> response, Retrofit retrofit) {

                progressDialog.dismiss();
                isDataFetched = true;

                if (response.code() == 200 && response.body().size() > 0) {
                    responseCode = response.code();
                    mRestModel = response.body();
                    mAdapter = new RestaurantCustomAdapter(getActivity(), response.body(), mCurrentLocation);
                    mRecyclerView.setAdapter(mAdapter);
                    mAdapter.SetOnItemClickListener(new RestaurantCustomAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position, String lat, String lng) {


                            Bundle bundle = new Bundle();
                            bundle.putString("desLatitude", lat);
                            bundle.putString("desLongitude", lng);


                            if(mCurrentLocation!=null) {
                                bundle.putDouble("currentLatitude", mCurrentLocation.getLatitude());
                                bundle.putDouble("currentLongitude", mCurrentLocation.getLongitude());
                            }

                            startActivity(new Intent(getActivity(), MapActivity.class).putExtras(bundle));
                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "Error occurred,please try later", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void setUpUI(List<RestaurantModel> mRestModel) {

        mAdapter = new RestaurantCustomAdapter(getActivity(), mRestModel, mCurrentLocation);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.SetOnItemClickListener(new RestaurantCustomAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, String lat, String lng) {


                Bundle bundle = new Bundle();
                bundle.putString("desLatitude", lat);
                bundle.putString("desLongitude", lng);
                bundle.putDouble("currentLatitude", mCurrentLocation.getLatitude());
                bundle.putDouble("currentLongitude", mCurrentLocation.getLongitude());

                startActivity(new Intent(getActivity(), MapActivity.class).putExtras(bundle));
            }
        });
    }

    private String getAddressFromLocation(double lat, double lng) {

        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        StringBuilder sb = null;

        try {
            sb = new StringBuilder();
            List<Address> addressList = geocoder.getFromLocation(lat, lng, 1);
            if (addressList != null) {
                Address address = addressList.get(0);

                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    if (address.getAddressLine(0) != null) {
                        sb.append(address.getAddressLine(0));
                    }
                    if (address.getAddressLine(1) != null) {
                        sb.append(address.getAddressLine(1));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sb.toString();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_retry:
                if (Utilities.checkInternetConnection(getActivity())) {
                    onLoadData();
                    lytNoInternet.setVisibility(View.GONE);

                }

        }
    }

    public void setCurrentLocation(Location location) {
        mCurrentLocation = location;
        String address = getAddressFromLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        if (!TextUtils.isEmpty(address))
            txtCurrentAddress.setText(address);
        if (mAdapter != null)
            mAdapter.locationChanged(location);
    }

}