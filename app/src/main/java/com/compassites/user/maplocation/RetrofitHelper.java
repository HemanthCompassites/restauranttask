package com.compassites.user.maplocation;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by user on 5/9/2016.
 */
public class RetrofitHelper {
    public static final String BASE_URL = "https://lit-sea-66228.herokuapp.com";


    public static Retrofit getRetrofitBuilder() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;

    }
}
