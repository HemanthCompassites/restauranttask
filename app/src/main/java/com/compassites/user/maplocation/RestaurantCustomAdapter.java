package com.compassites.user.maplocation;

import android.content.Context;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 5/4/2016.
 */
public class RestaurantCustomAdapter extends RecyclerView.Adapter<RestaurantCustomAdapter.RestaurantViewHolder> {

    OnItemClickListener mItemClickListener;
    List<RestaurantModel> mRestModel;
    Context context;
    Location mLocation;

    public RestaurantCustomAdapter(Context context, List<RestaurantModel> mRestModel, Location location) {
        this.mRestModel = mRestModel;
        this.context = context;
        mLocation = location;
    }

    @Override
    public int getItemCount() {
        return mRestModel.size();
    }

    @Override
    public RestaurantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lyt_restaurant_adapter, parent, false);

        RestaurantViewHolder dataObjectHolder = new RestaurantViewHolder(view);
        return dataObjectHolder;

    }

    @Override
    public void onBindViewHolder(RestaurantViewHolder holder, int position) {

        holder.resName.setText(mRestModel.get(position).getRestaurant_name());
        holder.resAddress.setText(mRestModel.get(position).getAddress());
        holder.resPhone.setText(mRestModel.get(position).getPhone_number());
        Picasso.with(context).load(mRestModel.get(position).getLogo_url()).into(holder.resImage);

        try {
            Location resLocation = new Location("");
            resLocation.setLatitude(Double.parseDouble(mRestModel.get(position).getLatitude()));
            resLocation.setLongitude(Double.parseDouble(mRestModel.get(position).getLongitude()));


            if (mLocation != null) {
                float distance = mLocation.distanceTo(resLocation) / 1000;

                String value = String.format(Locale.getDefault(), "%.2f", distance);
                StringBuilder sb = new StringBuilder();
                sb.append(value + " km");

                holder.resDistance.setText(sb.toString());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public class RestaurantViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView resName, resAddress, resPhone, resDistance;
        ImageView resImage;

        public RestaurantViewHolder(View view) {
            super(view);

            resName = (TextView) view.findViewById(R.id.txt_name);
            resAddress = (TextView) view.findViewById(R.id.txt_address);
            resPhone = (TextView) view.findViewById(R.id.txt_phone);
            resDistance = (TextView) view.findViewById(R.id.txt_distance);
            resImage = (ImageView) view.findViewById(R.id.iv_restaurant);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.onItemClick(v, getAdapterPosition(), mRestModel.get(getAdapterPosition()).getLatitude(), mRestModel.get(getAdapterPosition()).getLongitude());
        }

    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position, String lat, String lng);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void locationChanged(Location location) {
        mLocation = location;
        notifyDataSetChanged();
    }

}
